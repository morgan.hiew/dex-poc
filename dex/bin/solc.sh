#!/usr/bin/env bash

in="$@"

# `solc` v0.4.20 installed via `shell.nix`
# eg.  /nix/store/jvksw50m3z480ycl8v4xp2jzsskl1paf-solc-0.4.20
solc=solc

# `solc` v0.4.21 installed via `brew install solidity`
#solc=/usr/local/bin/solc

out=./out/out.json
err=./out/err.json
sum=./out/out.b2
new=./out/contracts.json
colors=$(dirname "$0")/.solc-colors
now=$(date +%H:%M:%S)

cd $(dirname "$0")/..
test -d ./out || mkdir -p ./out
test -f $out -o -f $err && rm -f $out $err

which $solc
$solc --version
cat solc-input.json | $solc --standard-json --allow-paths . 2>$err  | jq . >$out

echo -ne "\n$now "

if jq -e '.contracts|any' $out > /dev/null; then
    if test -f $sum && b2sum --status --check $sum; then
        echo "Not modified"
        exit 0
    else
        contract_names=$(jq -j '.contracts|keys|join(" ")' $out)
        echo "Successfully compiled:"
        echo "  $contract_names"
        cp $out $new
        b2sum $out > $sum
    fi
else
    echo "Errors:"
fi

jq -r '(.errors//[])[].formattedMessage' $out | grcat $colors > /dev/stderr
test -s $err && (echo; cat $err) > /dev/stderr
