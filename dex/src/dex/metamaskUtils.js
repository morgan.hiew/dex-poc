/// /////////////////////////////////////
// NAVBAR INTERFACE
/// /////////////////////////////////////

import { dontObserve } from './utils.js'
import { address, balance, allowance, wad2D } from '../lib/chain-dsl/es6.js'
import { config } from '../config.js'

const mmPollIntervalMs = 2000

export function init ({store, web3, mmProvider, contracts}) {
  const moduleName = 'metamaskUtils'

  const moduleDef = {
    state () {
      const initialState = {
        web3,
        mmProvider,
        network: [],
        currentAccount: null,
        signInError: null,
        contracts,
        tokens: null
      }
      dontObserve(initialState, 'web3')
      dontObserve(initialState, 'contracts')
      return initialState
    },

    getters: {
      getCurrentAccount,
      getSignInError,
      getContracts,
      getTokens,
      requiredNetwork: () => config
    },

    mutations: {
      setCurrentAccount,
      setSignInError,
      setTokens
    },

    actions: {
      fetchCurrentAccountPeriodic,
      fetchCurrentBalance,
      start
    },

    namespaced: true
  }

  store.registerModule(moduleName, moduleDef)
}

/// /////////////////////////////////////
// VUEX GETTERS
/// /////////////////////////////////////

function getSignInError (state) {
  return state.signInError
}

function getCurrentAccount (state) {
  return state.currentAccount
}

function getContracts (state) {
  return state.contracts
}

function getTokens (state) {
  return state.tokens
}

/// /////////////////////////////////////
// VUEX MUTATIONS
/// /////////////////////////////////////

function setCurrentAccount (state, currentAccount) {
  state.currentAccount = currentAccount
}

function setSignInError (state, error) {
  state.signInError = error
}

function setTokens (state, tokens) {
  state.tokens = tokens
}

/// /////////////////////////////////////
// VUEX ACTIONS
/// /////////////////////////////////////

function start ({state, commit, dispatch}) {
  dispatch('fetchCurrentAccountPeriodic')
}

async function fetchCurrentAccountPeriodic ({state, commit, dispatch}) {
  setInterval(() => fetchCurrentAccount({state, commit, dispatch}), mmPollIntervalMs)
}

async function fetchCurrentAccount ({state, commit, dispatch}) {
  try {
    // state.web3 assigned dex.mmWeb3 always exists
    if (!state.mmProvider) {
      commit('setSignInError', 'NO_METAMASK')
      return
    }

    let web3NetworkId = await state.web3.eth.net.getId()

    if (web3NetworkId !== config.networkId) {
      commit('setSignInError', 'WRONG_NETWORK')
      return
    }

    let accounts = await state.web3.eth.getAccounts()

    if (accounts === undefined || accounts.length === 0) {
      commit('setSignInError', 'METAMASK_LOCKED')
      return
    }

    let me = accounts[0]
    detectAccountChange({state, commit, dispatch, me})
  } catch (e) {
    commit('setCurrentAccount', null)
    commit('setSignInError', e.message)
  }
}

function detectAccountChange ({state, commit, dispatch, me}) {
  const current = getCurrentAccount(state)
  const newAccount = current === null
  const changedAccount = newAccount ? false : current !== me
  if (newAccount) {
    handleAccountChange({commit, dispatch, me}, false)
  } else if (changedAccount) {
    handleAccountChange({commit, dispatch, me}, true)
  }
  dispatch('fetchCurrentBalance')
}

function handleAccountChange ({commit, dispatch, me}, reload = false) {
  console.debug('Metamask account detected.')
  commit('setCurrentAccount', me)
  commit('setSignInError', null)
  dispatch('fetchCurrentBalance')
  if (reload) {
    window.location.reload()
  }
}

async function tokenStatus (token, proxy, account) {
  return {
    balance: wad2D(await balance(token, account)),
    approved: D(await allowance(token, account, address(proxy))).gt(0),
    contract: token
  }
}

async function fetchCurrentBalance ({state, commit}) {
  // console.debug('fetchCurrentBalance')
  try {
    const {
      web3,
      contracts: { weth, oax, swimusd, proxy },
      currentAccount
    } = state

    if (currentAccount) {
      let tokens = []
      let eth = {
        symbol: 'ETH',
        token: 'ETH',
        text: 'ETH',
        balance: wad2D(await balance(web3, currentAccount)),
        approved: false,
        contract: null
      }

      tokens.push(eth)

      let wethToken = {
        symbol: 'WETH',
        token: 'WETH',
        text: 'WETH - Wrapped Ether',
        ...(await tokenStatus(weth, proxy, currentAccount))
      }

      tokens.push(wethToken)

      let oaxToken = {
        symbol: 'OAX',
        token: 'OAX',
        text: 'OAX - Open Asset Exchange',
        ...(await tokenStatus(oax, proxy, currentAccount))
      }

      tokens.push(oaxToken)

      let swimusdToken = {
        symbol: 'SWIMUSD',
        token: 'SWIMUSD',
        text: 'SWIM USD',
        ...(await tokenStatus(swimusd, proxy, currentAccount))
      }

      tokens.push(swimusdToken)

      commit('setTokens', tokens)
    } else {
      commit('setTokens', null)
    }
  } catch (e) {
    console.debug('fetchCurrentBalance error', e)
    commit('setSignInError', e.message)
  }
}
