export class TradeIntent {
  constructor ({intentType, address, buy, sell}) {
    this.intentType = intentType

    this.address = address

    this.buy = {
      amount: D(buy.amount),
      token: buy.token
    }

    this.sell = {
      amount: D(sell.amount),
      token: sell.token
    }
  }
}

export function create (params) {
  return new TradeIntent(params)
}

export function fromPrice ({amount, price, tokenPair, intentType, address}) {
  const intent = {buy: {}, sell: {}, address, intentType}
  const [base, quote] = (intentType === 'buy') ? ['buy', 'sell'] : ['sell', 'buy']

  intent[base].token = tokenPair.base
  intent[base].amount = D(amount)
  intent[quote].token = tokenPair.quote
  intent[quote].amount = D(amount).times(D(price))

  return new TradeIntent(intent)
}
