import { tokenAddressesToHash, tokenPairToHash, struct } from '../utils.js'
import * as Order from '../Order.js'

export const TYPES = {
  ORDER: 'order',
  RFOB: 'rfob'
}

export const VERSION = 1

export function isOrderMessage (message) {
  return message.type === TYPES.ORDER
}

export function isRFOBMessage (message) {
  return message.type === TYPES.RFOB
}

export const Message = struct(
  {
    type: struct.enum(R.values(TYPES)),
    tokens: 'hash',
    data: 'object',
    version: 'number'
  },
  // DEFAULTS
  {
    version: VERSION
  }
)

export function toRFOBMessage (tokenPair) {
  return Message({
    type: TYPES.RFOB,
    data: {},
    tokens: tokenPairToHash(tokenPair)
  })
}

export function toOrderMessage (signedOrder) {
  return Message({
    type: TYPES.ORDER,
    data: signedOrder,
    tokens: tokenAddressesToHash(Order.tokensAddress(signedOrder))
  })
}
