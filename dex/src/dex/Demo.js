import {
  address,
  call,
  send,
  approve,
  balance,
  D2wad,
  wad2D,
  transfer,
  allowance
} from 'chain-dsl/es6'

async function ensureEther (web3, src, dst, low, high) {
  console.debug(`ensureEther`, {src, dst, low, high})
  const initialBalance = wad2D(await balance(web3, dst))
  if (initialBalance.lt(low)) {
    const topup = high.minus(initialBalance)
    return transfer(web3, src, dst, D2wad(topup))
  } else {
    return initialBalance
  }
}

async function ensureWETH (web3, guy, weth, low, high) {
  console.debug(`ensureWETH`, {guy, wethToken: address(weth), low, high})
  const initialBalance = wad2D(await balance(weth, guy))
  if (initialBalance.lt(low)) {
    const topup = high.minus(initialBalance)
    return transfer(web3, guy, address(weth), D2wad(topup))
  } else {
    return initialBalance
  }
}

async function ensureToken (token, owner, guy, low, high) {
  const symbol = Web3.utils.hexToUtf8(await call(token, 'symbol'))
  console.debug(`ensureToken`, {guy, token: address(token), symbol, low, high})
  const initialBalance = wad2D(await balance(token, guy))
  if (initialBalance.lt(low)) {
    const topup = high.minus(initialBalance)
    return send(token, owner, 'transfer', guy, D2wad(topup))
  } else {
    return initialBalance
  }
}

async function fundAccounts (web3, {weth, oax, swimusd}, DEPLOYER, ALICE, BOB) {
  console.debug('fundAccounts', {
    weth: address(weth),
    oax: address(oax),
    swimusd: address(swimusd),
    DEPLOYER,
    ALICE,
    BOB
  })

  await ensureEther(web3, DEPLOYER, ALICE, D`3`, D`5`)
  await ensureEther(web3, DEPLOYER, BOB, D`3`, D`5`)
  await ensureWETH(web3, ALICE, weth, D`1`, D`3`)
  await ensureToken(oax, DEPLOYER, BOB, D('1' + '000'), D('10' + '000'))
  await ensureToken(swimusd, DEPLOYER, BOB, D('2' + '000'), D('20' + '000'))
}

// To avoid some approve transactions which can slow tests down,
// we only approve again when allowance is below 10% of the desired one
async function ensureAllowance (token, owner, spender, newAllowance) {
  const currentAllowance = wad2D(await allowance(token, owner, address(spender)))
  if (currentAllowance.isLessThan(newAllowance.multipliedBy(D`0.1`))) {
    await approve(token, owner, address(spender), D2wad(newAllowance))
  }
}

export async function ensure (
  web3, deployedContracts, {DEPLOYER, ALICE, BOB}) {
  const {weth, oax, swimusd, proxy} = deployedContracts

  await fundAccounts(web3, deployedContracts, DEPLOYER, ALICE, BOB)
  await ensureAllowance(weth, ALICE, proxy, D('3'))
  await ensureAllowance(oax, BOB, proxy, D('9' + '000'))
  await ensureAllowance(swimusd, BOB, proxy, D('20' + '000'))
  return {contracts: deployedContracts, accounts: {DEPLOYER, ALICE, BOB}}
}
