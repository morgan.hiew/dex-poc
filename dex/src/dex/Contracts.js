function newContract (web3, savedContract) {
  const {name, from, address, jsonInterface} = savedContract
  return new web3.eth.Contract(jsonInterface, address, {
    gas: 4e6,
    // gasPrice: web3.utils.toWei('1', 'gwei'),
    name,
    from
  })
}

export async function load (loader, web3) {
  return {
    proxy: newContract(web3, await loader('proxy')),
    xchg: newContract(web3, await loader('xchg')),
    weth: newContract(web3, await loader('weth')),
    oax: newContract(web3, await loader('oax')),
    swimusd: newContract(web3, await loader('swimusd')),
    zrx: newContract(web3, await loader('zrx'))
  }
}
