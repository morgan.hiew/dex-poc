import { VuexMutations } from './VuexMutations.js'

export function VuexMutationsPayload (store, watchedMutations) {
  return VuexMutations(store, watchedMutations)
    .map(({mutation}) => mutation.payload)
}
