import { wad2D } from '../lib/chain-dsl/es6.js'

/// /////////////////////////////////////
// COMPONENT LIFECYCLE
/// /////////////////////////////////////

export function create ({store, web3, contractEvents}) {
  const moduleName = 'tradeHistory'

  return {
    module: moduleName,
    store,
    web3,
    contractEvents,
    subscriptions: null
  }
}

export function start (tradeHistory) {
  const {
    store,
    module
  } = tradeHistory

  const subscriptions = []

  store.registerModule(module, {
    state () {
      return {
        pastTransactions: [],
        oneMinHistory: [],
        oneDayHistory: []
      }
    },
    getters: {
      historySortedByTimestamp,
      getOneMinHistory,
      getOneDayHistory
    },
    mutations: {
      addPastTransaction,
      setOneMinHistory
    },
    namespaced: true
  })

  subscriptions.push(monitorOrders(tradeHistory))

  tradeHistory = R.assoc('subscriptions', subscriptions, tradeHistory)

  return tradeHistory
}

export function stop (tradeHistory) {
  tradeHistory.subscriptions.forEach(s => s.unsubscribe())
  tradeHistory.store.unregisterModule(tradeHistory.module)
}

export function pastTransactions (tradeHistory) {
  return tradeHistory.store.state[tradeHistory.module].pastTransactions
}

/// /////////////////////////////////////
// VUEX GETTERS
/// /////////////////////////////////////

function historySortedByTimestamp (state) {
  const records = [...state.pastTransactions]
  records.sort((a, b) => b.timestamp - a.timestamp)

  // add up property to indicate price change compared to previous one.
  records.forEach((r, i) => {
    let next = records[i + 1]
    if (next !== undefined && r.price > next.price) {
      r.up = true
    }
  })
  return records
}

function getOneMinHistory (state) {
  return R.sortBy(R.prop('timestamp'), state.oneMinHistory)
}

function getOneDayHistory (state) {
  return R.sortBy(R.prop('timestamp'), state.oneDayHistory)
}
/// /////////////////////////////////////
// VUEX MUTATIONS
/// /////////////////////////////////////

function addPastTransaction (state, transaction) {
  if (!state.pastTransactions.some(t => t.transactionHash === transaction.transactionHash)) {
    state.pastTransactions.push(transaction)

    // group transactions by its timestamp's min
    let timeKey = (x) => `${x.getFullYear()}-${x.getMonth() + 1}-${x.getDate()} ${x.getHours()}:${x.getMinutes()}:00`

    // reset oneMinHistory each time there's new data
    state.oneMinHistory = []

    Rx.Observable
      .from(state.pastTransactions)
      .groupBy(x => timeKey(x.timestamp))
      .flatMap(group => group.pipe(Rx.operators.toArray()))
      // calculate OPHL in the same group
      .map((x) => {
        let sorted = R.sort((a, b) => a.price - b.price, x)
        return [ x[0], R.last(x), sorted[0], R.last(sorted) ]
      })
      // set current interval's open price as last interval's close price
      .scan((x, y) => {
        y[0] = x[1]
        return y
      })
      .subscribe(x => {
        // coz Open price  is from last interval, so use Close price's timestamp
        state.oneMinHistory.push({
          tradeDate: timeKey(x[1].timestamp),
          timestamp: x[1].timestamp.getTime(),
          ophl: x.map(i => i.price.toFixed(8))})
      }, e => console.log(e))

    // group transactions by its timestamp's day
    let oneDayTimeKey = (x) => `${x.getFullYear()}-${x.getMonth() + 1}-${x.getDate()}`
    // reset oneMinHistory each time there's new data
    state.oneDayHistory = []

    let oneDayGroup = Rx.Observable
      .from(state.pastTransactions)
      .groupBy(x => oneDayTimeKey(x.timestamp))

    oneDayGroup
      .flatMap(group => group.pipe(Rx.operators.toArray()))
      // calculate OPHL in the same group
      .map((x) => {
        let sorted = R.sort((a, b) => a.price - b.price, x)
        return [ x[0], R.last(x), sorted[0], R.last(sorted) ]
      })
      // set current interval's open price as last interval's close price
      .scan((x, y) => {
        y[0] = x[1]
        return y
      })
      // zip one day volume
      .zip(oneDayGroup.flatMap(group => group.reduce((acc, x) => x.amount.plus(acc), D`0`)))
      .subscribe(x => {
        let [ophl, volumn] = x

        state.oneDayHistory.push({
          // coz Open price  is from last interval, so use Close price's timestamp
          tradeDate: timeKey(ophl[1].timestamp),
          timestamp: ophl[1].timestamp.getTime(),
          ophl: ophl.map(i => i.price.toFixed(8)),
          volumn: volumn
        })
      }, e => console.error(e))
  }
}

function setOneMinHistory (state, history) {
  state.oneMinHistory = history
}

/// /////////////////////////////////////
// INTERNAL FUNCTIONS
/// /////////////////////////////////////

function monitorOrders (tradeHistory) {
  const {
    web3,
    store,
    contractEvents,
    module
  } = tradeHistory

  return processEvents(contractEvents, {module, store, web3})
}

function processEvents (contractEvents, {module, store, web3}) {
  const {base, quote} = store.state.selectedPair
  const tokenPairHash = Web3.utils.soliditySha3(quote.address, base.address)

  // covers both sell and buy orders
  const tokensHashes = [
    Web3.utils.soliditySha3(quote.address, base.address),
    Web3.utils.soliditySha3(base.address, quote.address)
  ]

  return contractEvents
    .filter(R.propEq('event', 'LogFill'))
    .filter(event => tokensHashes.includes(event.returnValues.tokens))
    .mergeMap(toTransaction(web3, tokenPairHash))
    .subscribe(transaction => {
      store.commit(`${module}/addPastTransaction`, transaction)
    },
    err => console.error(err)
    )
}

function toTransaction (web3, tokenPairHash) {
  return event => {
    const values = event.returnValues
    const takerAmount = wad2D(values.filledTakerTokenAmount)
    const makerAmount = wad2D(values.filledMakerTokenAmount)

    // maker token is quote, therefore the taker is a buy order
    const isBuy = values.tokens === tokenPairHash

    const block$ = Rx.Observable.fromPromise(
      web3.eth.getBlock(event.blockNumber)
    )

    return block$
      .mergeMap(block => {
        const transaction = {
          price: isBuy ? makerAmount.div(takerAmount) : takerAmount.div(makerAmount),
          amount: isBuy ? takerAmount : makerAmount,
          orderHash: values.orderHash,
          timestamp: new Date(block.timestamp * 1000),
          transactionHash: event.transactionHash,
          event
        }

        return Rx.Observable.of(transaction)
      })
  }
}
