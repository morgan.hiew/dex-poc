/**
 * Blotter Functions
 *
 * This assumes that the blotter object implements the state management interface of Vuex
 *
 * @module dex/blotter/vuex_datastore
 * @see {@link https://vuex.vuejs.org/en/|Vuex documentation}
 */

import * as BlotterStorage from './BlotterStorage.js'
import { mapMutation } from './utils.js'
import { OrderExpiry } from './streams/OrderExpiry.js'
import { VuexMutationsPayload } from './streams/VuexMutationsPayload.js'
import * as Order from './Order.js'
import * as Fill from './Fill.js'

/// /////////////////////////////////////
// BLOTTER INTERFACE
/// /////////////////////////////////////

export function create ({
  store,
  storage,
  netId,
  moduleName = 'blotter',
  account,
  sendMessageChannel,
  receiveMessageChannel
}) {
  const moduleDef = {
    state () {
      return {
        orders: []
      }
    },
    getters: {
      getRecord,
      getRecordsSortedByAge
    },
    mutations: {
      addRecord,
      updateRecord
    },
    namespaced: true
  }
  store.registerModule(moduleName, moduleDef)

  const orderStorage = BlotterStorage.create({
    store,
    storage,
    netId,
    keyPrefix: moduleName,
    addOrder: 'blotter/addRecord',
    saveOn: ['blotter/addRecord', 'blotter/updateRecord'],
    statePath: [moduleName, 'orders']
  })

  const orderExpirySource = VuexMutationsPayload(store, ['blotter/addRecord'])

  return {
    store,
    moduleName,
    storage,
    netId,
    orderStorage,
    orderExpirySource,
    account,
    sendMessageChannel,
    receiveMessageChannel,
    subscriptions: []
  }
}

export function start (blotter) {
  let { store, orderStorage, orderExpirySource, netId, subscriptions } = blotter

  // Start this before order storage
  const orderExpirySubscription = OrderExpiry(orderExpirySource).subscribe(
    order => {
      store.commit('blotter/updateRecord', order)
    }
  )

  subscriptions.push(orderExpirySubscription)

  orderStorage = { ...orderStorage, netId }
  orderStorage = orderStorage.init()

  return { ...blotter, orderExpirySubscription, orderStorage }
}

export function stop (blotter) {
  blotter.orderStorage.destroy()
  blotter.subscriptions.forEach(s => s.unsubscribe())
  return blotter
}

function state (blotter) {
  return blotter.store.state[blotter.moduleName]
}

export function orders (blotter) {
  return state(blotter).orders
}

export function get (blotter, orderHash) {
  return orders(blotter).find(o => o.recordHash === orderHash)
}

export function contains (blotter, order) {
  return orders(blotter).some(o => o.recordHash === order.orderHash)
}

export const insert = mapMutation('blotter/addRecord')
export const update = mapMutation('blotter/updateRecord')

export function toRecord (record) {
  const isFillRecord = R.isNil(record.orderHash)

  let wrappedRecord

  if (isFillRecord) {
    if (R.isNil(record.transactionHash)) {
      throw Error('Transaction Hash is not available for fill')
    }

    wrappedRecord = {
      recordHash: record.transactionHash,
      type: 'fill',
      data: record
    }
  } else {
    wrappedRecord = {
      recordHash: record.orderHash,
      type: 'order',
      data: record
    }
  }

  return wrappedRecord
}

/// /////////////////////////////////////
// VUEX GETTERS
/// /////////////////////////////////////

function getRecordsSortedByAge (state) {
  return (account, tokenPair) => {
    return state.orders
      .map(o => {
        const fillOrOrder = o.data
        const data = Order.withPrice(fillOrOrder, tokenPair)

        return merge(o, { data })
      })
      .filter(o => {
        if (account) {
          const orderOrFill = o.data
          const addressOfOrder = R.path(['address'], orderOrFill)
          const addressOfFillIntent = R.path(
            ['fillIntent', 'address'],
            orderOrFill
          )

          return addressOfOrder === account || addressOfFillIntent === account
        }

        return true
      })
      .filter(o => Order.hasTokenPair(o.data, tokenPair))
  }
}

function getRecord (state) {
  return orderHash =>
    state.orders.find(o => {
      return o.recordHash === orderHash
    })
}

/// /////////////////////////////////////
// VUEX MUTATIONS
/// /////////////////////////////////////

function addRecord (state, orderOrFill) {
  const wrappedRecord = toRecord(orderOrFill)

  if (state.orders.some(o => o.recordHash === wrappedRecord.recordHash)) {
    throw Error('Record already exists in blotter')
  }

  return state.orders.push(wrappedRecord)
}

function updateRecord (state, orderOrFill) {
  const wrappedRecord = toRecord(orderOrFill)

  const orders = state.orders
  const orderIndex = R.findIndex(
    R.propEq('recordHash', wrappedRecord.recordHash)
  )(orders)

  if (orderIndex === -1) return false

  const foundRecord = orders[orderIndex]

  const updatedOrder = R.mergeDeepRight(foundRecord, wrappedRecord)

  if (updatedOrder.type === 'order') {
    Object.setPrototypeOf(updatedOrder.data.buy.amount, BigNumber.prototype)
    Object.setPrototypeOf(updatedOrder.data.sell.amount, BigNumber.prototype)
    updatedOrder.data = Order.create(updatedOrder.data)
  } else if (updatedOrder.type === 'fill') {
    Object.setPrototypeOf(
      updatedOrder.data.fillIntent.buyAmt,
      BigNumber.prototype
    )
    Object.setPrototypeOf(
      updatedOrder.data.fillIntent.sellAmt,
      BigNumber.prototype
    )
    updatedOrder.data = Fill.create(updatedOrder.data)
  } else {
    throw Error('Invalid blotter record type')
  }

  state.orders = R.update(orderIndex, updatedOrder, orders)
}
