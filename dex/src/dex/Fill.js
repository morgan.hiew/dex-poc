import {
  now,
  wad2D,
  distillEvent
} from '../lib/chain-dsl/es6.js'

export const ZeroExErrors = [
  'ORDER_EXPIRED',
  'ORDER_FULLY_FILLED_OR_CANCELLED',
  'ROUNDING_ERROR_TOO_LARGE',
  'INSUFFICIENT_BALANCE_OR_ALLOWANCE'
]

export class Fill {
  constructor ({fillIntent, transactionHash, events = []}) {
    const amountsToD = {
      buyAmt: D,
      sellAmt: D
    }

    this.fillIntent = R.evolve(amountsToD, fillIntent)
    this.transactionHash = transactionHash
    // {event, error}
    this.events = events

    Object.freeze(this)
  }

  get status () {
    const filledSell = this.filled.sell
    const lastEvent = R.last(this.events)

    if (lastEvent) {
      if (lastEvent.status === 'error' ||
        lastEvent.status === 'expired' ||
        lastEvent.status === 'cancelled') {
        return lastEvent.status
      }
    }

    if (filledSell.gte(this.fillIntent.sellAmt)) {
      return 'fulfilled'
    } else if (filledSell.gt(D`0`)) {
      return 'partiallyFilled'
    } else if (this.fillIntent.order.exp > now()) {
      return 'pendingConfirmation'
    } else {
      return 'expired'
    }
  }

  get error () {
    if (this.events.length === 0) {
      return null
    }
    return R.last(this.events).error
  }

  get filled () {
    const amounts = R.map((event) => filledAmounts(event, this), this.events)
    const filled = {
      'buy': sum(R.map(R.prop('buy'), amounts)),
      'sell': sum(R.map(R.prop('sell'), amounts))
    }
    return filled
  }

  get buy () {
    return {
      amount: this.fillIntent.buyAmt,
      token: this.fillIntent.order.sell.token

    }
  }

  get sell () {
    return {
      amount: this.fillIntent.sellAmt,
      token: this.fillIntent.order.buy.token
    }
  }

  get exp () {
    return this.fillIntent.order.exp
  }
}

export function create (params) {
  return new Fill(params)
}

export function filled (fill) {

}

function filledAmounts (event) {
  // This is an fill so we are the taker
  // TakerToken is the token the taker has (sells).
  const sell = wad2D(R.path(['returnValues', 'filledTakerTokenAmount'], event) || '0')
  const buy = wad2D(R.path(['returnValues', 'filledMakerTokenAmount'], event) || '0')

  return {buy, sell}
}

// TODO dupe
function sum (numbers) {
  // could not make this work with R.reduce
  return numbers.reduce((acc, val) => acc.plus(val), D`0`)
}

function decodeExchangeError (errorId) {
  const ExchangeErrors = [
    'ORDER_EXPIRED - Order has already expired',
    'ORDER_FULLY_FILLED_OR_CANCELLED - Order has already been fully filled or cancelled',
    'ROUNDING_ERROR_TOO_LARGE - Rounding error too large',
    'INSUFFICIENT_BALANCE_OR_ALLOWANCE - Insufficient balance or allowance for token transfer'
  ]
  return ExchangeErrors[errorId] || 'Unknown Exchange error'
}

function containsEvent (event, fill) {
  return fill.events.find(e => e.transactionHash === event.transactionHash)
}

export async function updateFromEvent (event, fill) {
  if (containsEvent(event, fill)) { return fill }

  let newFill = R.clone(fill)
  Object.setPrototypeOf(newFill, Object.getPrototypeOf(fill))

  let parsedEvent = event

  if (event.event === 'LogError') {
    parsedEvent = parseErrorEvent(event)
  }

  newFill.events.push(parsedEvent)

  return newFill
}

function parseErrorEvent (event) {
  const distilledError = distillEvent(event)
  const errorId = distilledError.errorId
  distilledError.message = decodeExchangeError(errorId)

  const error = ZeroExErrors[errorId]

  let status
  if (error === 'ORDER_EXPIRED') {
    status = 'expired'
  } else {
    status = 'error'
  }

  return {...event, error: distilledError, status}
}
