/*
* This is a CommonJS module.
*
* It loads the `esm` library, which makes it possible for Node.js
* to understand subsequent EcmaScript6 modules directly,
* without any pre-processing.
*
* It must also establish the same global environment what `index.html`
* sets up with the *synchronously* loading <script> tags.
*
* It's called `index.js` to indicate it's purpose to be similar to `index.html`,
* BUT it's meant to be loaded in Node.js
*
* The purpose of setting the same global is to allow loading ES6 frontend
* code directly.
*
* There are 2 distinct use-cases where having the same global environment
* available is helpful, convenient and less error-prone:
*
* 1. node repl (bin/node.sh)
*       It allows copy-pasting code between the browser and node
*
* 2. mocha tests
*       It reduces the amount of repeating boilerplate required for tests
*
* Not all frontend code worth testing though, hence it's sufficient to setup
* only those libraries which are used by application logic.
*
* Some libraries (eg. `web3`) might have slightly different implementations
* for the browser and for Node.js, BUT they provide the same API.
* For such libraries, it's important to make sure they are on the same version.
*
* */

const {Web3, BigNumber} = require('chain-dsl')

// ==== 3rd party, non-ES6 modules ====
global.Web3 = Web3
global.BigNumber = BigNumber
global.Eth = require('ethjs')
global.R = require('ramda')
global.Vue = require('vue')
global.Vuex = require('vuex')
global.Rx = require('@reactivex/rxjs')
global.fetch = require('node-fetch')
Vue.use(Vuex)

// Enable loading ES6 modules using `require`
// Source of the tecnique:
//   https://github.com/standard-things/create-esm/blob/master/create-esm#L4-L5
require = require('esm')(module) // eslint-disable-line no-global-assign

// The following modules are the equivalent of those modules
// which are loaded in the browser via `main.js` (instead of `index.html`,
// because they are ES6 modules) and they are exposed as globals
// on the `window` object.

const component = require('./component')

global.clone = component.clone
global.merge = component.merge
global.start = component.start
global.stop = component.stop

// configuration
const { devChainUrl, anotherDevChainUrl } = require('./dex/utils.js')
global.devChainUrl = process.env.DEV_CHAIN_URL || devChainUrl
global.anotherDevChainUrl = process.env.ANOTHER_DEV_CHAIN_URL || anotherDevChainUrl
global.devChainWsUrl = process.env.DEV_CHAIN_WS_URL || 'ws://localhost:8901'
