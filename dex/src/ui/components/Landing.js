export default {
  name: 'landing',

  // language=HTML
  template: `
    <div class="landing">
      <div class="landing--container" >

        <div style="flex: 60; display: flex; flex-direction: column; justify-content: space-around;">
          <div id="landing--title">OAX DEX Prototype - 2018 </div>
          <div style="font-size: 22px;">The OAX Foundation is excited to release the first proof-of-concept (PoC) of the OAX project. The goal of the OAX project is to create a decentralized exchange platform that applies some of the latest blockchain innovations to revolutionalize digital asset trading. </div>
          <div style="display: flex; justify-content: space-between">
            <div id="landing--tutorial">
              <div style="width: 200px; height: 50px; font-size: 16px;">For a step-by-step tutorial, please click</div>
              <a style="cursor: pointer" @click="centerDialogVisible = true" >TUTORIAL</a>
            </div>

            <div id="landing--prototype">
              <div style="width: 200px; height: 50px; font-size: 16px;">To access the OAX prototype, please click</div>
              <router-link to="market">PROTOTYPE</router-link>
            </div>
          </div>
          <div>
            <h2 >*Disclaimer</h2>
            <p>The prototype available on this website does not involve any real digital assets. This prototype is meant for demonstration purposes and does not reflect every aspect of actual trading.
              The information contained in this website and prototype is for general information purposes only. We make no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, suitability or availability with respect to the website, the prototype or the information or related graphics contained within. Any reliance you place on such information is strictly at your own risk.</p>
          </div>
        </div>
        <div id="landing--divider" style="flex: 5"></div>
        <div id="landing--image" style="flex: 35">
          <img style="max-width: 100%" src="images/landing-img@2x.png">
        </div>

      </div>
      <div class="landing--footer">
      </div>
      <div class="landing--footer-inner">
        <div style="height: 2px; margin-top: 20px; background-color:white; "></div>
        <div style="display: flex; justify-content: space-between;">
          <div style="width: 200px; height: 100%; display: flex; flex-direction: column; justify-content: space-between;">
            <div style="width: 250px; height: 30px;">For more information about OAX:</div>
            <div class="links">
              <div>Website:</div>
              <div><a href="https://www.oax.org">OAX.ORG</a></div>
            </div>
            <div class="links">
              <div>whitepaper:</div>
              <div> <a  href="https://www.oax.org/assets/whitepaper/openANX_White_Paper_ENU.pdf">whitepaper</a></div>
            </div>
            <div class="links">
              <div>Email:</div>
              <div><a  href="mailto:info@oax.org">info@oax.org</a></div>
            </div>
          </div>
          <div>
            <div>OPEN</div>
            <div>TRANSPARENT</div>
            <div>DECENTRALIZED</div>
          </div>
        </div>
        <div style="display: flex; justify-content: flex-end;">
        </div>
      </div>

      <el-dialog  :visible.sync="centerDialogVisible" width="900px" center>
        <div>
          <h1 style="color: black; text-align: center;">Welcome to Video Tutorial</h1>
          <el-card class="box-card">
            <div slot="header" class="clearfix">
              <h2>1. Installing and Signing up MetaMask</h2>
            </div>
            <iframe width="800" height="450" src="https://www.youtube.com/embed/W6fgaJcYcbY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </el-card>

          <br/>
          <el-card class="box-card">
            <div slot="header" class="clearfix">
              <h2>2. Obtaining Test Ether</h2>
            </div>
            <iframe width="800" height="450" src="https://www.youtube.com/embed/pWoczGXSnIk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </el-card>

          <br/>
          <el-card class="box-card">
            <div slot="header" class="clearfix">
              <h2>3. Approving Tokens and Wrapping Ether</h2>
            </div>
            <iframe width="800" height="450" src="https://www.youtube.com/embed/1JGoz7JgxtY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </el-card>

          <br/>
          <el-card class="box-card">
            <div slot="header" class="clearfix">
              <h2>4.Filling a Sell Order (Buying)</h2>
            </div>
            <iframe width="800" height="450" src="https://www.youtube.com/embed/VMcBXBRUF0w" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </el-card>

          <br/>
          <el-card class="box-card">
            <div slot="header" class="clearfix">
              <h2>5. Placing a Buy Order</h2>
            </div>
            <iframe width="800" height="450" src="https://www.youtube.com/embed/MseDUx7I2Bo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </el-card>
        </div>
      </el-dialog>
    </div>`,

  data: function () {
    return {
      centerDialogVisible: false,
      version: null
    }
  }
}
