export default {
  template: `<div class="panel panel--history">
    <h3>HISTORY</h3>
    <table class="table">
        <thead class="table__header">
        <tr>
            <th>AMOUNT</th>
            <th>PRICE</th>
            <th>TIME</th>
        </tr>
        </thead>
        <tbody class="table__body">
        <tr v-for="r in historySortedByTimestamp">
            <td>{{r.amount.toFixed(8)}}</td>
            <td  v-bind:style="{ 'color': r.up? '#1CF477': '#FF5800' }" style="text-align: right;" >{{r.price.toFixed(8)}}</td>
            <td>{{r.timestamp.toLocaleDateString()}} {{r.timestamp.toLocaleTimeString()}}</td>
        </tr>
        </tbody>
    </table>
</div>`,

  computed: Vuex.mapGetters('tradeHistory', [
    'historySortedByTimestamp'
  ])
}
