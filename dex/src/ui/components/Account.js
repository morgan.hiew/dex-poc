import {D2wad} from '../../lib/chain-dsl/es6.js'
export default {
  name: 'account',

  template: `
    <div class="">
      <div class="account">
        <div class="">
          <table style="width: 100%">
            <thead>
            <tr>
              <th>Asset</th>
              <th>Available Balance</th>
              <th>Approved</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="item in getTokens " :key="item.symbol">
              <td>
                <h1 class="symbol">{{item.symbol}}</h1>
                <!--<div class="token">{{item.token}}</div>-->
              </td>
              <td>
                <h2>{{item.balance.toFixed(8)}}</h2>
              </td>
              <td v-if="item.contract">
                <el-button v-if="!item.approved" icon="el-icon-close" size="mini" circle @click="toggleApprove(item)" class="account-btn"></el-button>
                <el-button v-if="item.approved" icon="el-icon-check" size="mini" circle @click="toggleApprove(item)"  class="account-btn account-approved-btn" ></el-button>
              </td>
            </tr>

            </tbody>
          </table>


        </div>
      </div>

    </div>
  `,

  data: () => {
    return {
    }
  },

  methods: {
    toggleApprove: async function (token) {
      // use a big amount of approve amount
      let allowance = token.approved ? '0' : '-1'
      await token.contract.methods.approve(
        this.getContracts.proxy._address, D2wad(allowance))
        .send({from: this.getCurrentAccount})
    }
  },

  computed: {
    ...Vuex.mapGetters('metamaskUtils', [
      'getCurrentAccount',
      'getSignInError',
      'getContracts',
      'getTokens'
    ])
  }
}
