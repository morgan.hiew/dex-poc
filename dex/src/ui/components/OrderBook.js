import { floor, ceil } from '../../lib/chain-dsl/es6.js'
import * as Order from '../../dex/Order.js'
import {
  getFillableMakerTokenAmountAsync,
  availableTakerTokenAmountAsync
} from '../../Dex.js'
import * as Orderbook from '../../dex/Orderbook.js'

async function fillableBuyOrders (orders) {
  if (orders.length === 0) return []

  const uniqueAddressesForMatches = R.uniq(orders.map(o => o.address))

  const tokenContract = Order.findContract(dex.contracts, orders[0].sell.token.address) // eslint-disable-line no-undef

  const fillableMakerTokenAmounts = new Map(await Promise.all(
    uniqueAddressesForMatches.map(async a =>
      getFillableMakerTokenAmountAsync(a, tokenContract, dex.contracts.proxy) // eslint-disable-line no-undef
    )
  ))

  const availableTakerTokenAmounts = new Map(await Promise.all(
    orders.map(async o => availableTakerTokenAmountAsync(dex, o))) // eslint-disable-line no-undef
  )

  const fillableOrders =
    Order.availableForFill(orders, fillableMakerTokenAmounts, availableTakerTokenAmounts)
      .filter(o => !(o.fillableSellAmt.eq(D`0`) || o.fillableBuyAmt.eq(D`0`)))
      .map(({order, fillableSellAmt, fillableBuyAmt}) => {
        const fillableOrder = Order.create(R.omit(['price'], order))
        fillableOrder.sell.amount = fillableSellAmt
        fillableOrder.buy.amount = fillableBuyAmt
        return Order.withPrice(fillableOrder, dex.selectedPair) // eslint-disable-line no-undef
      })

  return Orderbook.aggregateOrderByPrice(fillableOrders).sort(
    (a, b) => parseFloat(b.price.amount) - parseFloat(a.price.amount)
  )
}

async function fillableSellOrders (orders) {
  if (orders.length === 0) return []

  const uniqueAddressesForMatches = R.uniq(orders.map(o => o.address))

  const tokenContract = Order.findContract(dex.contracts, orders[0].sell.token.address) // eslint-disable-line no-undef

  const fillableMakerTokenAmounts = new Map(await Promise.all(
    uniqueAddressesForMatches.map(async a =>
      getFillableMakerTokenAmountAsync(a, tokenContract, dex.contracts.proxy) // eslint-disable-line no-undef
    )
  ))

  const availableTakerTokenAmounts = new Map(await Promise.all(
    orders.map(async o => availableTakerTokenAmountAsync(dex, o))) // eslint-disable-line no-undef
  )

  const fillableOrders =
    Order.availableForFill(orders, fillableMakerTokenAmounts, availableTakerTokenAmounts)
      .filter(o => !(o.fillableSellAmt.eq(D`0`) || o.fillableBuyAmt.eq(D`0`)))
      .map(({order, fillableSellAmt, fillableBuyAmt}) => {
        const fillableOrder = Order.create(R.omit(['price'], order))
        fillableOrder.sell.amount = fillableSellAmt
        fillableOrder.buy.amount = fillableBuyAmt
        return Order.withPrice(fillableOrder, dex.selectedPair) // eslint-disable-line no-undef
      })

  return Orderbook.aggregateOrderByPrice(fillableOrders).sort(
    (a, b) => parseFloat(b.price.amount) - parseFloat(a.price.amount)
  )
}

export default {
  data () {
    return {
      sellOrders: [],
      buyOrders: []
    }
  },
  template: `<div class="panel panel--orderbook">
    <h3>ORDER BOOK</h3>

    <table class="table">
        <thead>
        <tr>
            <th colspan="2" class="table__header-cell">AMOUNT</th>
            <th class="table__header-cell">PRICE</th>
        </tr>
        </thead>
        <tbody class="table__body">
        <tr v-for="order of sellOrders" data-order-type="sell" @click="prefillOrder(order)">
            <td style="height: 9px; width: 35px"><div style="background-color: #FF5800; height: 9px"  v-bind:style="{ 'margin-left': (100-percentage(order.sell.amount)) + '%' }" ></div></td>
            <td class="table__number-cell">{{order.sell.amount.toFixed(8)}}</td>
            <td class="table__number-cell">{{ceil(order.price.amount)}}</td>
        </tr>
        <tr style="background-color: rgba(255,255,255,0.1); height: 30px">
            <td colspan="2" style="text-align: left;">&nbsp;</td>
            <td colspan="2"> <div style="text-align: left;" v-if="sortedSellOrders.length>0 && sortedBuyOrders.length>0">&nbsp;</div></td>
        </tr>
         <tr v-for="order of buyOrders" data-order-type="buy" @click="prefillOrder(order)">
            <td style="height: 9px; width: 35px"><div style="background-color: #1CF477; height: 9px"  v-bind:style="{ 'margin-left': (100-percentage(order.buy.amount)) + '%' }" ></div></td>
            <td class="table__number-cell">{{order.buy.amount.toFixed(8)}}</td>
            <td class="table__number-cell">{{floor(order.price.amount)}}</td>
        </tr>
        </tbody>
    </table>
</div>`,

  computed: Vuex.mapGetters([
    'sortedBuyOrders',
    'sortedSellOrders'
  ]),

  async mounted () {
    this.buyOrders = await fillableBuyOrders(
      this.$store.getters['sortedBuyOrders']
    )
    this.sellOrders = await fillableSellOrders(
      this.$store.getters['sortedSellOrders']
    )
  },

  watch: {
    async sortedBuyOrders (orders) {
      this.buyOrders = await fillableBuyOrders(orders)
    },
    async sortedSellOrders (orders) {
      this.sellOrders = await fillableSellOrders(orders)
    }
  },

  methods: {
    floor (amount) {
      return floor(amount, 8)
    },

    ceil (amount) {
      return ceil(amount, 8)
    },

    percentage (amount) {
      // Todo fix hard coded max price
      return Math.min(amount.dividedBy(0.01).multipliedBy(100).toNumber(), 100)
    },

    prefillOrder (order) {
      this.$store.commit('setOrderPrice', order.price.amount)
    },

    spread (sortedSellOrders, sortedBuyOrders) {
      return R.last(sortedSellOrders).price.amount.minus(sortedBuyOrders[0].price.amount).toFixed(8)
    }
  }
}
