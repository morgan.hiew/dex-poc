import { D2wad } from '../../lib/chain-dsl/es6.js'

export default {
  name: 'wrapeth',

  template: `
<div class="wrap panel" style="display: flex; justify-content: space-between;" >
    <div style="display: flex; flex-direction: column; align-items: center; margin: 0 5px">
        <div><h3>Wrap ETH</h3></div> 
        <p>Wrapped Ether, or WETH, is a tradeable version of regular Ether. Be sure to keep some regular ETH to pay misc. gas costs.</p>
        <div style="align-self: stretch; display: flex; justify-content: space-between; font-size: 20px">
            <div>ETH</div>
            <div>{{balanceOf('ETH')}}</div>
        </div>
        <div style="align-self: stretch; display: flex; justify-content: space-between;">
            <input placeholder="0" v-model="wrapAmt">
        </div>
        <button v-on:click="wrap()">Convert to WETH</button>
    </div>
    <div style="display: flex; flex-direction: column; align-items: center; margin: 0 5px">
        <div><h3>Unwrap WETH</h3></div> 
        <p>You can unwrap your WETH back to ETH anytime. Any WETH you convert back to ETH will no longer be usable on OAX dex.
        </p>
        <div style="align-self: stretch; display: flex; justify-content: space-between; font-size: 20px">
            <div>WETH</div>
            <div>{{balanceOf('WETH')}}</div>
        </div>
        <div style="align-self: stretch; display: flex; justify-content: space-between;">
            <input placeholder="0" v-model="unWrapAmt">
        </div>
        <button v-on:click="unWrap()">Convert to ETH</button>
    </div>
    
</div>
    `,

  data: () => {
    return {
      unWrapAmt: 0,
      wrapAmt: 0
    }
  },

  methods: {
    wrap: async function () {
      await this.getContracts.weth.methods.deposit()
        .send({
          from: this.getCurrentAccount,
          value: D2wad(D(this.wrapAmt))
        })
    },

    unWrap: async function () {
      await this.getContracts.weth.methods.withdraw(D2wad(D(this.unWrapAmt))).send({from: this.getCurrentAccount})
    },

    balanceOf: function (symbol) {
      if (this.getTokens != null) {
        return R.find(R.propEq('symbol', symbol))(this.getTokens).balance.toPrecision(8)
      } else {
        return null
      }
    }
  },

  computed: {
    ...Vuex.mapGetters('metamaskUtils', [
      'getCurrentAccount',
      'getSignInError',
      'getContracts',
      'getTokens',
      'getEth'
    ])
  }
}
