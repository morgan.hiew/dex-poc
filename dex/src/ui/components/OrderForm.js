export default {
  data: () => {
    return {
      activeTab: 'limit-order'
    }
  },

  template: `<div class="panel panel--orderform" style="position: relative">

    <div style="width: 100%; height: 100%; position: absolute; z-index:1000; background-color: rgba(0,0,0,0.4); display: flex; justify-content: center; align-items: center;" v-if="!getCurrentAccount">

        <div style="width: 50%; height: 50%; background-color:#FFC400; color: black; padding: 10px; ">
            <h1>Connect your wallet</h1>
            <h2>You are viewing this in read-only mode. Connect your wallet and start to trade! </h2>
            <h2><a style="color: black" href="#/tutorial">Tutorial</a></h2>
        </div>
    </div>

    <div v-if="activeTab === 'limit-order'" class="tab-content">

        <form  class="background-highlighted" v-on:submit.prevent="placeOrder">
            <div v-bind:style="{ 'background-color': buying ? '#1CF477':'#FF5800' }" style="height: 4px"></div>
            <div class="order-actions">

                <button class="btn btn--buy"
                        :class="{'is-active': buying}"
                        type="button"
                        @click="setOrderType('buy')"
                        style="text-align: right">Buy
                </button>
                <div style="width: 4px;background-color: rgba(255,255,255,0.2)"></div>
                <button class="btn btn--sell"
                        :class="{'is-active': selling}"
                        type="button"
                        @click="setOrderType('sell')"
                        style="text-align: left">Sell
                </button>
            </div>

            <div style="display: flex; justify-content: space-around; margin-bottom: 2rem;">
                <div style="width: 40%">
                     <h2 class="orderform__label">Amount</h2>
                     
                     <div class="order-input-box">
                    <input type="number" title="Amount" class="order-amount no-spinners"
                   @blur="setOrderAmount($event.target.value)" placeholder="0" min="0"
                   step="0.000000000000000001"
                   required>
                   <div class="order-input-box__token">{{selectedPair.base.symbol}}</div>
                   </div>

                </div>
                <div style="width: 40%">
                    <h2 class="orderform__label">Price</h2>
                    <div class="order-input-box">
                    <input type="number" title="Price" class="order-amount no-spinners"
                   @blur="setOrderPrice($event.target.value)" placeholder="0" v-bind:value="orderPrice"
                   step="0.000000000000000001"
                   min="0" required>
                   <div class="order-input-box__token">{{selectedPair.quote.symbol}}</div>
                   </div>
                </div>
            </div>

            <div style="width: 90%; margin: 0 auto;">
                <div  style="height: 1px; background-color: white"></div>
                <div style="display: flex; justify-content: space-between; font-size: 20px">
                    <div>Approx. total:</div>
                     <div>{{orderTotal.toFixed(8)}} {{selectedPair.quote.symbol}}</div>
                </div>
            </div>

            <div style="display: flex; justify-content: center">
                <el-button :class="limitOrderConfirmClasses" @click="placeOrder">{{limitOrderConfirmLabel}}</el-button>
            </div>
        </form>
    </div>
</div>
    `,

  methods: {
    changeTabTo (tab) { this.activeTab = tab },

    ...Vuex.mapMutations([
      'setOrderAmount',
      'setOrderPrice',
      'setOrderType'
    ]),

    ...Vuex.mapActions([
      'placeOrder'
    ])
  },

  computed: {
    buying () { return this.orderType === 'buy' },
    selling () { return this.orderType === 'sell' },

    limitOrderConfirmClasses () {
      return this.buying ? 'btn--confirm-buy' : 'btn--confirm-sell'
    },

    limitOrderConfirmLabel () {
      return this.buying ? 'Place Buy Order' : 'Place Sell Order'
    },

    ...Vuex.mapGetters([
      'orderTotal'
    ]),

    ...Vuex.mapState([
      'selectedPair',
      'orderType',
      'orderPrice'
    ]),
    ...Vuex.mapGetters('metamaskUtils', [
      'getCurrentAccount'
    ])
  }
}
