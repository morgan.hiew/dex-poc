/*
function calculateMA (dayCount, data) {
  let result = []
  for (let i = 0, len = data.values.length; i < len; i++) {
    if (i < dayCount) {
      result.push('-')
      continue
    }
    let sum = 0
    for (let j = 0; j < dayCount; j++) {
      sum += data.values[i - j][1]
    }
    result.push(+(sum / dayCount).toFixed(3))
  }
  return result
}
*/

export default {
  template: `<div class="panel panel--chart" style=" display: flex; flex-direction: column; align-items: stretch;">
    <div style="flex: 1;"><h3> Price Chart</h3></div>
    <div id="chart" style="flex: 9" class="chart"></div>
</div>`,
  data: function () {
    return {
      myChart: null,
      trades: []
    }
  },
  methods: {
    updateChart: function () {
      let option = {
        backgroundColor: '#1A1A1A',
        tooltip: {
          trigger: 'axis',
          axisPointer: {
            type: 'cross'
          }
        },
        grid: {
          top: '5%',
          left: '1%',
          right: '5%',
          bottom: '15%'
        },
        xAxis: {
          type: 'category',
          data: this.trades.tradeDate,
          scale: true,
          boundaryGap: false,
          axisLine: {onZero: false, lineStyle: {color: '#8B8B8B'}},
          splitLine: {show: false},
          splitNumber: 20,
          min: 'dataMin',
          max: 'dataMax'
        },
        yAxis: {
          scale: true,
          axisTick: {show: false},
          position: 'right',
          splitLine: {show: true, lineStyle: {color: '#3E3E3E'}},
          splitArea: {
            show: false
          },
          axisLine: {show: false, lineStyle: {color: '#fff'}}
        },
        axisPointer: {
          link: {xAxisIndex: 'all'},
          label: {
            backgroundColor: '#000'
          }
        },
        dataZoom: [
          {
            type: 'inside',
            start: 50,
            end: 100
          }
        ],
        series: [
          {
            name: 'Candlestick',
            type: 'candlestick',
            data: this.trades.values,
            itemStyle: {
              normal: {
                color: '#1A1A1A',
                color0: '#FF5800',
                borderColor: '#0CF49B',
                borderColor0: '#FF5800'
              }
            }
          }
        ]
      }
      this.myChart.setOption(option)
    }
  },
  mounted: function () {
    this.$nextTick(function () {
      this.myChart = echarts.init(document.getElementById('chart'))
    })
  },

  computed: {
    ...Vuex.mapGetters('tradeHistory', [
      'getOneMinHistory'
    ])
  },

  watch: {
    getOneMinHistory: function (newHistory, oldHistory) {
      this.trades = {tradeDate: newHistory.map(x => x.tradeDate), values: newHistory.map(x => x.ophl)}
      this.updateChart()
    }
  }
}
