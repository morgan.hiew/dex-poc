import { web3Contract } from './lib/chain-dsl/es6.js'
import { Lifecycle } from './component.js'

export class ContractsComponent extends Lifecycle {
  constructor ({Loader, contractNames, /* DEPS */ web3} = {}) {
    super()
    Object.assign(this, {Loader, contractNames, web3})
  }

  async start () {
    let started = clone(this)
    const {Loader, contractNames, web3} = started
    const networkId = await web3.eth.net.getId()
    const load = Loader(networkId)

    const loadedContracts = await Promise.all(contractNames.map(load))
    const web3Contracts = loadedContracts.map(c => web3Contract(web3, c))
    Object.assign(started, R.zipObj(contractNames, web3Contracts))
    return started
  }

  async stop () {
    this.contractNames.forEach(contractName => delete this[contractName])
    return this
  }
}
