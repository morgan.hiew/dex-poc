import fs from 'fs'
import path from 'path'
import { load } from 'chain-dsl/json'

export function NodejsContractLoader (netId) {
  return async (contractName) => {
    const jsonInterfaceFileName = path.join(__dirname, '..', 'net',
      netId.toString(), `${contractName}.json`)
    if (fs.existsSync(jsonInterfaceFileName)) {
      return load(jsonInterfaceFileName)
    } else {
      throw Error(
        `Contract JSON interface doesn't exist: ${jsonInterfaceFileName}`)
    }
  }
}
