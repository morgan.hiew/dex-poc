/* eslint-env mocha */

import Storage from 'dom-storage'
import sinon from 'sinon'

import { expect } from 'chain-dsl/test/helpers'
import {
  D2wad,
  wad2D,
  ZERO_ADDR,
  address,
  allowance,
  now,
  waitForConnection,
  waitForReceipt,
  decodeLogs
} from 'chain-dsl/es6'
import { NodejsContractLoader as ContractLoader } from '../../src/NodejsContractLoader'
import * as Contracts from '../../src/dex/Contracts'
import * as Demo from '../../src/dex/Demo'
import * as Order from '../../src/dex/Order'
import * as Fill from '../../src/dex/Fill'
import * as TradeIntent from '../../src/dex/TradeIntent'
import * as CancelIntent from '../../src/dex/CancelIntent'
import * as ContractLog from '../../src/dex/ContractLog'
import { config } from '../../src/config'

function mkOrder (from, buyAmt, buyToken, sellAmt, sellToken) {
  return Order.create({
    address: from,
    buy: {amount: buyAmt, token: buyToken},
    sell: {amount: sellAmt, token: sellToken}
  })
}

describe('Order', function () {
  let web3, contracts, DEPLOYER, ALICE, BOB, OAX, WETH, proxy, oax, weth

  // helper functions for tests
  async function settleIntent (tradeIntent, order) {
    const [fillIntent] = Order.matchIntentWithMakerOrders(
      tradeIntent, [{order, fillableSellAmt: order.sell.amount}]
    )
    const transactionHash = await Order.settle(web3, contracts, fillIntent)
    return Fill.create({fillIntent, transactionHash})
  }

  function settleOrder (order) {
    const intent = {
      address: order.address === ALICE ? BOB : ALICE,
      intentType: 'buy',
      sell: order.buy,
      buy: order.sell
    }
    return settleIntent(intent, order)
  }

  function cancelOrder (order, amount) {
    const cancelAmt = R.isNil(amount) ? order.buy.amount : amount
    const cancelIntent = CancelIntent.create({order, cancelAmt})
    return Order.cancel(web3, contracts, cancelIntent)
  }

  function createOrder (overrides) {
    return Order.create({
      buy: {amount: D`0.0001`, token: OAX},
      sell: {amount: D`0.0001`, token: WETH},
      address: ALICE,
      ...overrides
    })
  }

  async function signedOrder (overrides) {
    return Order.sign(web3, contracts, createOrder({
      buy: {amount: D`0.0001`, token: OAX},
      sell: {amount: D`0.0001`, token: WETH},
      address: ALICE,
      ...overrides
    }))
  }

  async function updateFromReceipt (txn, order, contracts) {
    const event = txn.events.LogError || txn.events.LogCancel || txn.events.LogFill
    return Order.updateFromEvent(event, order, contracts)
  }

  async function updateFillFromReceipt (txn, fill) {
    const event = txn.events.LogError || txn.events.LogCancel || txn.events.LogFill
    return Fill.updateFromEvent(event, fill)
  }

  before('deployment', async () => {
    web3 = new Web3(devChainUrl)
    await waitForConnection(web3)
    ;[DEPLOYER, ALICE, BOB] = await web3.eth.getAccounts()
    const loader = ContractLoader(await web3.eth.net.getId())
    const deployedContracts = await Contracts.load(loader, web3)
    ;({contracts} = await Demo.ensure(web3, deployedContracts, {DEPLOYER, ALICE, BOB}))
    ;({proxy, oax, weth} = contracts)
    OAX = {symbol: 'OAX', address: address(oax), name: 'Open Asset Exchange'}
    WETH = {symbol: 'WETH', address: address(weth), name: 'Wrapped Ether'}
  })

  describe('.create', () => {
    it('returns an Order instance', async () => {
      const order = Order.create({
        buy: {amount: D`1`, token: OAX},
        sell: {amount: D`1`, token: WETH},
        address: ZERO_ADDR
      })
      expect(order).is.an.instanceOf(Order.Order)
    })

    it('validates params', function () {
      expect(() => { createOrder({exp: 'exp'}) }).to.throw('Expected a value of type `number` for `exp`')
      expect(() => { createOrder({salt: 'salt'}) }).to.throw('Expected a value of type `number` for `salt`')
      expect(() => { createOrder({orderHash: 'aHash'}) }).to.throw('Expected a value of type `hash | undefined` for `orderHash`')
      expect(() => { createOrder({buy: {}}) }).to.throw('Expected a value of type `orderAmount` for `buy.amount`')
      expect(() => { createOrder({buy: {amount: D`1`}}) }).to.throw('Expected a value of type `{address,name,symbol}` for `buy.token`')
      expect(() => { createOrder({sell: {}}) }).to.throw('Expected a value of type `orderAmount` for `sell.amount`')
      expect(() => { createOrder({sell: {amount: D`1`}}) }).to.throw('Expected a value of type `{address,name,symbol}` for `sell.token`')
      expect(() => { createOrder({address: undefined}) }).to.throw('Expected a value of type `string` for `address`')
      expect(() => { createOrder({sig: 'aSig'}) }).to.throw('Expected a value of type `signature | undefined` for `sig`')
    })

    it('converts string amounts to decimals ', async () => {
      const order = Order.create({
        buy: {amount: D`123`, token: OAX},
        sell: {amount: D`123`, token: WETH},
        address: ZERO_ADDR
      })

      expect(order).nested.property('buy.amount').instanceOf(BigNumber)
      expect(order).nested.property('sell.amount').instanceOf(BigNumber)
    })
  })

  describe('createFromPrice', function () {
    it('with price and buy amount it calculates sell amount', () => {
      expect(Order.createFromPrice({
        price: D`1.2`,
        intentType: 'buy',
        amount: D`3`,
        tokenPair: {base: WETH, quote: OAX},
        address: ZERO_ADDR
      })).deep.nested.property('sell.amount', D`3.6`)
    })

    it('with price and sell amount it calculates buy amount', () => {
      expect(Order.createFromPrice({
        price: D`1.2`,
        intentType: 'sell',
        amount: D`3.4`,
        tokenPair: {base: WETH, quote: OAX},
        address: ZERO_ADDR
      })).deep.nested.property('buy.amount', D`4.08`)
    })
  })

  describe('withPrice', function () {
    it('decorates an Order with price relative to given token pair', function () {
      const order = Order.create({
        buy: {amount: '123', token: OAX},
        sell: {amount: '246', token: WETH},
        address: ZERO_ADDR
      })

      expect(Order.withPrice(order, {base: WETH, quote: OAX}))
        .have.nested.property('price.amount').that.eqD(`0.5`)

      expect(Order.withPrice(order, {base: OAX, quote: WETH}))
        .have.nested.property('price.amount').that.eqD(`2`)
    })
  })

  describe('asArrays', function () {
    it('works', async () => {
      const makerOrder = Order.create({
        address: ALICE,
        buy: {amount: D`1`, token: OAX},
        sell: {amount: D`2`, token: WETH}
      })

      expect(Order.asArrays(makerOrder))
        .eql({
          orderAddresses: [
            ALICE,
            ZERO_ADDR, // Any taker
            WETH.address, // Token address of what the Maker has
            OAX.address, // Token address of what the Taker has
            ZERO_ADDR // No fee recipient
          ],
          orderValues: [
            D2wad(`2`),
            D2wad(`1`),
            toBN(0), // no maker fee
            toBN(0), // no taker fee
            toBN(makerOrder.exp),
            toBN(makerOrder.salt)
          ]
        })
    })
  })

  it('.hash', async () => {
    const order = createOrder()
    expect(Order.hash(contracts, order)).satisfy(Web3.utils.isHexStrict)
  })

  it('.sign', async () => {
    await expect(Order.sign(web3, contracts, createOrder()))
      .eventually.is.an.instanceOf(Order.Order)
      .that.have.property('sig')
  })

  describe('.updateFromEvent', () => {
    context('LogFill', function () {
      it('is handled', async () => {
        const order = createOrder()
        const event = {
          event: 'LogFill',
          returnValues: {
            feeRecipient: '0x0000000000000000000000000000000000000000',
            filledMakerTokenAmount: '100000000000000',
            filledTakerTokenAmount: '100000000000000',
            maker: ALICE,
            makerToken: '0x43952f32d7f21a10652F7a67245823B0A4621D9e',
            orderHash: '0x0b13341a233f7d749d9e11314fb0ced7e1403669c7d6b06e8e93f3475e3975e1',
            paidMakerFee: '0',
            paidTakerFee: '0',
            taker: BOB,
            takerToken: '0xcD82E4D2bB3EC5691d642dB429de09Cd8c3D0144',
            tokens: '0x3fa8af82d41767b960e1dc6813987e1accee6ec6ff20ac906a45e19adf61a0c6'
          }
        }
        const updatedOrder = Order.updateFromEvent(event, order, contracts)
        await expect(updatedOrder).eventually.property('status', 'fulfilled')
      })

      it('is idempotent', async () => {
        const order = createOrder()
        const event = {
          transactionHash: '0x6c1cacbe2e6dd6dec4e27fcbedb7d8028b7c04e1ff95ad35d86eb6991d9c7015',
          event: 'LogFill',
          returnValues: {
            feeRecipient: '0x0000000000000000000000000000000000000000',
            filledMakerTokenAmount: '100000000000000',
            filledTakerTokenAmount: '100000000000000',
            maker: ALICE,
            makerToken: '0x43952f32d7f21a10652F7a67245823B0A4621D9e',
            orderHash: '0x0b13341a233f7d749d9e11314fb0ced7e1403669c7d6b06e8e93f3475e3975e1',
            paidMakerFee: '0',
            paidTakerFee: '0',
            taker: BOB,
            takerToken: '0xcD82E4D2bB3EC5691d642dB429de09Cd8c3D0144',
            tokens: '0x3fa8af82d41767b960e1dc6813987e1accee6ec6ff20ac906a45e19adf61a0c6'
          }
        }
        const updatedOrder = await Order.updateFromEvent(event, order, contracts)
        const doublyUpdatedOrder = await Order.updateFromEvent(event, updatedOrder)
        expect(doublyUpdatedOrder.filled.buy).eqD(D`0.0001`)
        expect(doublyUpdatedOrder.filled.sell).eqD(D`0.0001`)
      })

      it('handles partial fill', async () => {
        // const order = createOrder({status: 'pendingConfirmation'})
        const order = createOrder()

        const event = {
          event: 'LogFill',
          returnValues: {
            feeRecipient: '0x0000000000000000000000000000000000000000',
            filledMakerTokenAmount: '3333333333333',
            filledTakerTokenAmount: '3333333333333',
            maker: ALICE,
            makerToken: '0x43952f32d7f21a10652F7a67245823B0A4621D9e',
            orderHash: '0x0b13341a233f7d749d9e11314fb0ced7e1403669c7d6b06e8e93f3475e3975e1',
            paidMakerFee: '0',
            paidTakerFee: '0',
            taker: '0xb357fc3DBD4CDb7cBD96AA0A0bD905dBE56CaB77',
            takerToken: '0xcD82E4D2bB3EC5691d642dB429de09Cd8c3D0144',
            tokens: '0x3fa8af82d41767b960e1dc6813987e1accee6ec6ff20ac906a45e19adf61a0c6'
          }
        }

        const updatedOrder = await Order.updateFromEvent(event, order, contracts)
        expect(updatedOrder.status).equal('partiallyFilled')
      })
    })

    context('LogCancel', function () {
      it('is handled', async () => {
        const order = createOrder()
        const event = {event: 'LogCancel'}

        await expect(Order.updateFromEvent(event, order, contracts))
          .eventually.have.property('status', 'cancelled')
      })
    })

    context('LogError', function () {
      function LogError (returnValues) {
        return {event: 'LogError', returnValues}
      }

      context('ORDER_EXPIRED', function () {
        it('is handled', async () => {
          const order = createOrder()
          const event = LogError({
            errorId: '0',
            orderHash: '0x0b13341a233f7d749d9e11314fb0ced7e1403669c7d6b06e8e93f3475e3975e1'
          })

          await expect(Order.updateFromEvent(event, order, contracts))
            .eventually.have.property('status', 'expired')
        })
      })

      context('ORDER_FULLY_FILLED_OR_CANCELLED', function () {
        it('is not handled for maker', async () => {
          const makerOrder = await signedOrder()

          const event = LogError({
            errorId: '1',
            orderHash: makerOrder.orderHash
          })

          await expect(Order.updateFromEvent(event, makerOrder, contracts))
            .eventually.property('status', 'active')
        })

        it('is handled for taker', async () => {
          const takerOrder = createOrder()

          const event = LogError({
            errorId: '1',
            orderHash: '[not maker order hash]'
          })

          expect(await Order.updateFromEvent(event, takerOrder, contracts))
            .property('status', 'error')
        })
      })

      context('ROUNDING_ERROR_TOO_LARGE', function () {
        it('is not handled for maker', async () => {
          const makerOrder = await signedOrder()

          const event = LogError({
            errorId: '2',
            orderHash: makerOrder.orderHash
          })

          await expect(Order.updateFromEvent(event, makerOrder, contracts))
            .eventually.have.property('status', 'active')
        })

        it('is handled for taker', async () => {
          const takerOrder = createOrder()
          const event = LogError({
            errorId: '2',
            orderHash: '0x0b13341a233f7d749d9e11314fb0ced7e1403669c7d6b06e8e93f3475e3975e1'
          })

          await expect(Order.updateFromEvent(event, takerOrder, contracts))
            .eventually.have.property('status', 'error')
        })
      })

      context('INSUFFICIENT_BALANCE_OR_ALLOWANCE', function () {
        it('is handled for maker', async () => {
          const ALICEallowance = wad2D(await allowance(weth, ALICE, address(proxy)))
          const makerOrder = await signedOrder({
            address: ALICE,
            buy: {amount: D`1`, token: OAX},
            sell: {amount: ALICEallowance.times(2), token: WETH}
          })

          const event = LogError({
            errorId: '3',
            orderHash: makerOrder.orderHash
          })

          await expect(Order.updateFromEvent(event, makerOrder, contracts))
            .eventually.have.property('status', 'error')
        })

        it('is not handled if order is not the maker', async () => {
          const takerOrder = createOrder({
            address: ALICE,
            orderHash: Web3.utils.randomHex(32)
          })

          const event = LogError({
            errorId: '3',
            orderHash: Web3.utils.randomHex(32)
          })

          await expect(Order.updateFromEvent(event, takerOrder, contracts))
            .eventually.have.property('status', 'active')
        })
      })
    })
  })

  describe('.settlementAmounts', () => {
    it('returns correct amounts for fully matching Orders', async () => {
      const makerOrder = await signedOrder({
        address: ALICE
      })

      const takerOrder = createOrder({
        address: BOB,
        sell: makerOrder.buy,
        buy: makerOrder.sell
      })

      const {buyAmt, sellAmt} = Order.settlementAmounts(
        makerOrder, makerOrder.sell.amount, takerOrder.buy.amount, takerOrder.sell.amount, 'buy'
      )

      expect(buyAmt).eqD(makerOrder.buy.amount)
      expect(sellAmt).eqD(takerOrder.buy.amount)
    })
  })

  describe('.settle', () => {
    it('sets status to pendingConfirmation while waiting for the fill', async () => {
      const makerOrder = await signedOrder({
        address: ALICE
      })

      const tradeIntent = {
        intentType: 'buy',
        address: BOB,
        sell: makerOrder.buy,
        buy: makerOrder.sell
      }

      const fill = settleIntent(tradeIntent, makerOrder)

      await expect(fill).eventually.property('transactionHash').satisfies(Web3.utils.isHexStrict)

      await expect(fill).eventually.property('status', 'pendingConfirmation')
    })

    it('handles fills', async () => {
      const makerOrder = await signedOrder({
        address: ALICE
      })

      const fill = await settleOrder(makerOrder)
      expect(fill.transactionHash).satisfies(Web3.utils.isHexStrict)
      expect(fill.status).equal('pendingConfirmation')
    })

    it('handles fills (why another test??)', async () => {
      const makerOrder = await signedOrder()

      const fill = await settleOrder(makerOrder)

      const receipt = await waitForReceipt(web3, fill.transactionHash)
      decodeLogs(contracts.xchg, receipt)

      expect(receipt)
        .nested.property('events.LogFill.returnValues.orderHash', makerOrder.orderHash)

      await expect(updateFromReceipt(receipt, makerOrder)).eventually
        .property('status', 'fulfilled')

      await expect(updateFillFromReceipt(receipt, fill)).eventually
        .property('status', 'fulfilled')
    })

    it('handles failure', async () => {
      const makerOrder = await signedOrder({
        address: ALICE,
        buy: {amount: D`0.7`, token: OAX},
        sell: {amount: D`1`, token: WETH}
      })

      const tradeIntent = {
        address: BOB,
        intentType: 'buy',
        buy: {amount: D`0.000000000000000005`, token: WETH},
        sell: {amount: D`1`, token: OAX}
      }

      const fill = await settleIntent(tradeIntent, makerOrder)
      const receipt = await waitForReceipt(web3, fill.transactionHash)

      decodeLogs(contracts.xchg, receipt)
      expect(receipt).nested.property('events.LogError')
      expect(receipt).nested.property('events.LogError.returnValues.orderHash',
        makerOrder.orderHash)

      await expect(updateFromReceipt(receipt, makerOrder)).eventually
        .property('status', 'active')

      const updatedFill = updateFillFromReceipt(receipt, fill)

      await expect(updatedFill).eventually.property('status', 'error')

      await expect(updatedFill).eventually
        .property('error')
        .that.eql({
          NAME: 'LogError',
          errorId: '2',
          message: 'ROUNDING_ERROR_TOO_LARGE - Rounding error too large',
          orderHash: makerOrder.orderHash
        })
    })

    it('handles failure with ContractEvents', async () => {
      const makerOrder = await signedOrder({
        address: ALICE,
        buy: {amount: D`0.7`, token: OAX},
        sell: {amount: D`1`, token: WETH}
      })

      const tradeIntent = {
        address: BOB,
        intentType: 'buy',
        buy: {amount: D`0.000000000000000005`, token: WETH},
        sell: {amount: D`1`, token: OAX}
      }

      const fill = await settleIntent(tradeIntent, makerOrder)

      const contractLog = ContractLog.create({
        web3,
        exchange: contracts.xchg,
        netId: config.networkId,
        storage: new Storage(null, {strict: true}),
        pollInterval: 20
      })
      await ContractLog.start(contractLog)
      const contractEvents = contractLog.events

      const event = await new Promise(resolve =>
        contractEvents
          .filter(e => e.returnValues.orderHash === makerOrder.orderHash)
          .first()
          .subscribe(resolve)
      )

      const updatedMakerOrder = Order.updateFromEvent(event, makerOrder, contracts)
      const updatedFill = Fill.updateFromEvent(event, fill, contracts)

      await expect(updatedMakerOrder).eventually.property('status', 'active')
      await expect(updatedFill).eventually.property('status', 'error')
      await expect(updatedFill).eventually.property('error').that.eql({
        NAME: 'LogError',
        errorId: '2',
        message: 'ROUNDING_ERROR_TOO_LARGE - Rounding error too large',
        orderHash: makerOrder.orderHash
      })

      ContractLog.stop(contractLog)
    })

    it('handles partial fill', async () => {
      const makerOrder = await signedOrder({
        address: ALICE
      })

      const tradeIntent = {
        address: BOB,
        intentType: 'buy',
        buy: {
          ...makerOrder.sell,
          amount: makerOrder.sell.amount.div(10)
        },
        sell: makerOrder.buy
      }

      const fill = await settleIntent(tradeIntent, makerOrder)

      const receipt = await waitForReceipt(web3, fill.transactionHash)
      decodeLogs(contracts.xchg, receipt)
      expect(receipt.events.LogFill.returnValues.orderHash).equal(Order.hash(contracts, makerOrder))

      await expect(updateFillFromReceipt(receipt, fill))
        .eventually.property('status', 'fulfilled')

      await expect(updateFromReceipt(receipt, makerOrder, contracts))
        .eventually.property('status', 'partiallyFilled')
    })

    context('on mainnet', async () => {
      let stub
      before(() => {
        stub = sinon.stub(web3.eth.net, 'getId').resolves(1)
      })
      after(() => {
        stub.restore()
      })

      it('fails', async () => {
        const makerOrder = await signedOrder({address: ALICE})
        await expect(settleOrder(makerOrder))
          .eventually.rejectedWith(/Not ready for mainnet yet/)
      })
    })
  })

  describe('.cancel', () => {
    let order

    beforeEach(async () => {
      order = await signedOrder()
    })

    it('cancels', async () => {
      const cancel = cancelOrder(order, order.buy.amount)
      await expect(cancel).eventually.satisfy(Web3.utils.isHexStrict)
    })

    it('cannot be settled after being fully cancelled', async () => {
      await cancelOrder(order, order.buy.amount)
      const fill = await settleOrder(order)

      const receipt = await waitForReceipt(web3, fill.transactionHash)
      decodeLogs(contracts.xchg, receipt)
      const updatedFill = updateFillFromReceipt(receipt, fill)

      await expect(updatedFill)
        .eventually.property('status', 'error')
    })

    it('reduces fill amount when partially cancelling order', async () => {
      const cancelAmt = order.buy.amount.div('4')
      await cancelOrder(order, cancelAmt)

      const fillAmt = order.buy.amount.minus(cancelAmt)
      const fill = await settleOrder(order)

      const receipt = await waitForReceipt(web3, fill.transactionHash)
      decodeLogs(contracts.xchg, receipt)

      const updatedFill = await updateFillFromReceipt(receipt, fill)
      expect(updatedFill).have.nested.property('filled.buy').that.eqD(fillAmt)

      const updatedOrder = await updateFromReceipt(receipt, order)
      expect(updatedOrder).have.nested.property('filled.buy').that.eqD(fillAmt)
    })

    it('sets order status to cancelled', async () => {
      const transactionHash = await cancelOrder(order, order.buy.amount)
      const receipt = await waitForReceipt(web3, transactionHash)

      decodeLogs(contracts.xchg, receipt)

      const updatedOrder = await updateFromReceipt(receipt, order)

      expect(updatedOrder).property('status', 'cancelled')
    })

    // cancelOrder can fail with
    //   - ORDER_EXPIRED
    //   - ORDER_FULLY_FILLED_OR_CANCELLED

    it('order status unchanged if already fulfilled', async () => {
      // fully settle order
      const fill = await settleOrder(order)

      // process receipt
      const firstReceipt = await waitForReceipt(web3, fill.transactionHash)
      decodeLogs(contracts.xchg, firstReceipt)
      const filledOrder = await updateFromReceipt(firstReceipt, order)
      expect(filledOrder).property('status', 'fulfilled')

      // try to cancel
      const transactionHash = await cancelOrder(order, order.buy.amount)
      const secondReceipt = await waitForReceipt(web3, transactionHash)
      decodeLogs(contracts.xchg, secondReceipt)

      const updatedOrder = updateFromReceipt(secondReceipt, filledOrder)
      await expect(updatedOrder).eventually.property('status', 'fulfilled')
    })

    it('order status unchanged if expired', async () => {
      const order = createOrder({exp: now()})

      // try to cancel
      const transactionHash = await cancelOrder(order, order.buy.amount)
      const receipt = await waitForReceipt(web3, transactionHash)
      decodeLogs(contracts.xchg, receipt)

      const updatedOrder = updateFromReceipt(receipt, order)
      await expect(updatedOrder).eventually.property('status', 'expired')
    })
  })

  describe('.matchingOrders', function () {
    it('handles 1 exact match', async () => {
      const pendingOrder =
        mkOrder('ALI', `1`, OAX, `2`, WETH)

      const takerOrder =
        mkOrder('BOB', `2`, WETH, `1`, OAX)

      const orderbook = [pendingOrder]

      const matches = Order.matchingOrders(orderbook, takerOrder)

      expect(matches).containSubset([pendingOrder])
    })

    it('sell to highest buyer, buy from lowest seller', async () => {
      const cheapOrder =
        mkOrder('ALI', `0.5`, OAX, `1`, WETH) // wants 0.5 OAX for 1 WETH

      const exactOrder =
        mkOrder('BOB', `1`, OAX, `1`, WETH) // wants 1 OAX for 1 WETH

      const expensiveOrder =
        mkOrder('CAT', `2`, OAX, `1`, WETH) // wants 2 OAX for 1 WETH

      // Den buys 3 WETH for 3 OAX
      //  or sells 3 OAX for 3 WETH
      const takerOrder = mkOrder('DEN', `3`, WETH, `3`, OAX) // price 1 weth/oax

      const expensiveFirstOrderbook = [expensiveOrder, exactOrder, cheapOrder]
      const cheapFirstOrderbook = [cheapOrder, exactOrder, expensiveOrder]

      expect(Order.matchingOrders(expensiveFirstOrderbook, takerOrder))
        .eql([cheapOrder, exactOrder])

      expect(Order.matchingOrders(cheapFirstOrderbook, takerOrder))
        .eql([cheapOrder, exactOrder])
    })
  })

  describe('.availableForFill', function () {
    it('works with one order', function () {
      const order = mkOrder('ALI', `1`, OAX, `2`, WETH)

      const fillableMakerTokenAmounts = new Map([['ALI', D`1`]])
      const availableTakerTokenAmounts = new Map([[order.orderHash, D`1`]])

      const [fillableOrder] = Order.availableForFill(
        [order],
        fillableMakerTokenAmounts,
        availableTakerTokenAmounts
      )
      expect(fillableOrder.fillableSellAmt).eqD(`1`)
    })
  })

  describe('.matchIntentWithMakerOrders', function () {
    let order, tradeIntent

    beforeEach(function () {
      order = mkOrder('ALI', `1`, OAX, `1`, WETH)
      tradeIntent = {
        address: 'BOB',
        intentType: 'buy',
        buy: order.sell,
        sell: order.buy
      }
    })

    it('return one match if matching exactly', function () {
      const fillIntents = Order.matchIntentWithMakerOrders(
        tradeIntent, [{order, fillableSellAmt: D`1`}]
      )
      expect(fillIntents.length).eq(1)
    })

    it('returns no matches with zero fillable maker amount', function () {
      const fillIntents = Order.matchIntentWithMakerOrders(
        tradeIntent, [{order, fillableSellAmt: D`0`}]
      )
      expect(fillIntents.length).eq(0)
    })
  })

  describe('when matching multiple orders', function () {
    // errror in makerOrderFromIntents?

    // Buy 2 oax @ 4 weth/oax
    // Buy 1 oax @ 2 weth/oax
    // Sell 6 oax @ 1 weth/oax
    // should result in
    // - fills
    //   - sold 2 oax for 8 weth
    //   - sold 1 oax for 2 weth
    // - broadcast
    //   - sell 3 oax for 3 weth

    const buyAmt1 = D`0.0002`
    const buyAmt2 = D`0.0001`
    const sellAmt = D`0.0006`

    const buyPrice1 = D`4`
    const buyPrice2 = D`2`
    const sellPrice = D`1`

    let makerOrder1, makerOrder2, takerIntent

    beforeEach(async () => {
      makerOrder1 = Order.create({
        address: ALICE,
        buy: {amount: buyAmt1, token: OAX},
        sell: {amount: buyAmt1.times(buyPrice1), token: WETH}
      })

      makerOrder2 = Order.create({
        address: ALICE,
        buy: {amount: buyAmt2, token: OAX},
        sell: {amount: buyAmt2.times(buyPrice2), token: WETH}
      })

      takerIntent = TradeIntent.create({
        intentType: 'sell',
        address: BOB,
        sell: {amount: sellAmt, token: OAX},
        buy: {amount: sellAmt.times(sellPrice), token: WETH}
      })
    })

    it('respects SELL intent and matches multiple orders correctly', async function () {
      const fillIntents = Order.matchIntentWithMakerOrders(
        takerIntent, [{order: makerOrder1, fillableSellAmt: makerOrder1.sell.amount},
          {order: makerOrder2, fillableSellAmt: makerOrder2.sell.amount}]
      )
      expect(fillIntents.length).eq(2)

      const [fill1, fill2] = fillIntents
      expect(fill1.buyAmt).eqD(D`0.0008`)
      expect(fill1.sellAmt).eqD(D`0.0002`)

      expect(fill2.buyAmt).eqD(D`0.0002`)
      expect(fill2.sellAmt).eqD(D`0.0001`)

      const [order] = Order.makerOrderFromIntents(takerIntent, fillIntents)
      expect(order.sell.amount).eqD(D`0.0003`)
      expect(order.buy.amount).eqD(D`0.0003`)
    })
  })

  it('WhisperOrder returns a new order with only 0x properties', function () {
    const order = createOrder()
    const whisperOrder = Order.WhisperOrder(order)
    const orderWithoutZeroExProps = R.pick(Order.WHISPER_FIELDS, order)

    expect(whisperOrder).to.eql(orderWithoutZeroExProps)
  })

  it('Order.WHISPER_FIELDS returns fields needed ', function () {
    expect(Order.WHISPER_FIELDS).to.eql([
      'address',
      'sell',
      'buy',
      'exp',
      'salt',
      'sig',
      'orderHash'
    ])
  })

  it('ensureValidOrder works')
})
