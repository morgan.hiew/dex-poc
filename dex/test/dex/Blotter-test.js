/* eslint-env mocha */

import { expect } from 'chain-dsl/test/helpers'
import { now, ZERO_ADDR } from 'chain-dsl/es6'
import Storage from 'dom-storage'
import * as Order from '../../src/dex/Order'
import { VuexMutationsPayload } from '../../src/dex/streams/VuexMutationsPayload'
import * as Blotter from '../../src/dex/Blotter'
import * as BlotterStorage from '../../src/dex/BlotterStorage'
import { makeChannel } from '../../src/dex/utils'

describe('Blotter', () => {
  let blotter, store, storage, order, signedOrder
  const netId = 9
  const oax = { symbol: 'OAX', address: Web3.utils.randomHex(20), name: 'OAX' }
  const weth = {
    symbol: 'WETH',
    address: Web3.utils.randomHex(20),
    name: 'WETH'
  }

  before(() => {
    order = Order.create({
      buy: { amount: D`1`, token: oax },
      sell: { amount: D`1`, token: weth },
      address: ZERO_ADDR
    })

    signedOrder = merge(order, {
      sig: '0x5369676e6564203078204f72646572',
      orderHash: Web3.utils.randomHex(32)
    })

    Object.freeze(signedOrder)
  })

  beforeEach(async () => {
    store = new Vuex.Store({ strict: true })
    storage = new Storage(null, { strict: true })
    blotter = Blotter.start(
      Blotter.create({
        store,
        storage,
        netId,
        sendMessageChannel: makeChannel(),
        receiveMessageChannel: makeChannel()
      })
    )
  })

  afterEach(() => Blotter.stop(blotter))

  describe('Order Management', () => {
    it('Initializes with an empty array', () => {
      expect(store.state.blotter.orders).to.be.an('array').that.is.empty // eslint-disable-line
    })

    it.skip('Rejects unsigned orders', () => {
      expect(() => store.commit('blotter/addRecord', order)).throw(
        /must have valid signature/
      )
    })

    it('Adds a valid signed record to the blotter store', () => {
      store.commit('blotter/addRecord', signedOrder)
      expect(store.state.blotter.orders[0]).to.eql(
        Blotter.toRecord(signedOrder)
      )
    })

    it('Rejects duplicate orders with the same hash', () => {
      store.commit('blotter/addRecord', signedOrder)
      expect(() => store.commit('blotter/addRecord', signedOrder)).throw(
        /already exists/
      )
    })

    it('Accepts multiple orders with different hash', () => {
      store.commit('blotter/addRecord', signedOrder)
      const orderWithDifferentHash = {
        ...signedOrder,
        orderHash: 'another hash'
      }
      store.commit('blotter/addRecord', orderWithDifferentHash)

      const records = store.state.blotter.orders

      expect(records).lengthOf(2)
    })

    it.skip('Newly added record has an active status', () => {
      store.commit('blotter/addRecord', signedOrder)
      expect(store.state.blotter.orders[0]).property('status', 'active')
    })

    it.skip('The status of a record is perserved when added', () => {
      const expiredRecord = { ...signedOrder, status: 'expired' }
      store.commit('blotter/addRecord', expiredRecord)

      expect(store.state.blotter.orders[0]).property('status', 'expired')
    })

    it('Retrieve record by order hash', () => {
      store.commit('blotter/addRecord', signedOrder)
      const retrievedOrder = store.getters['blotter/getRecord'](
        signedOrder.orderHash
      )
      expect(retrievedOrder).to.eql(Blotter.toRecord(signedOrder))
    })

    it('can update record by order hash', () => {
      expect(signedOrder.price).to.not.eql(D`9000`)

      store.commit('blotter/addRecord', signedOrder)
      const updateObj = { ...signedOrder, sell: { amount: D`2`, token: weth } }

      store.commit('blotter/updateRecord', updateObj)

      const retrievedOrder = store.getters['blotter/getRecord'](
        signedOrder.orderHash
      ).data
      expect(retrievedOrder).containSubset({ sell: { amount: D`2` } })
    })

    it('records are scheduled for expiry', done => {
      const unsubscribe = store.subscribe(mutation => {
        if (mutation.type === 'blotter/updateRecord') {
          const expiredOrder = Order.create(mutation.payload)
          expect(expiredOrder).to.have.property('status', 'expired')
          unsubscribe()
          done()
        }
      })

      // an immediately expiring order
      store.commit('blotter/addRecord', { ...signedOrder, exp: now() })
    })

    it('Updating a record does not overrides fields not set', () => {
      store.commit('blotter/addRecord', signedOrder)
      const filledOrder = R.assocPath(['buy', 'amount'], D`2`, signedOrder)
      store.commit('blotter/updateRecord', filledOrder)

      expect(store.state.blotter.orders[0].data).eql(filledOrder)
    })

    it.skip("Changes a record's status to 'filled' if the order is filled in full", async () => {
      store.commit('blotter/addRecord', signedOrder)
      const filledOrder = { ...signedOrder, filled: signedOrder.buy.amount }
      store.commit('blotter/updateRecord', filledOrder)

      expect(store.state.blotter.orders[0].status).eql('fulfilled')
    })

    context('getters', function () {
      it('sorts records by age', () => {
        store.commit('blotter/addRecord', signedOrder)

        const orderExpiringSooner = {
          ...signedOrder,
          sell: { amount: D`2`, token: weth },
          orderHash: 'sortedFirst'
        }

        store.commit('blotter/addRecord', orderExpiringSooner)

        const sortedOrders = store.getters['blotter/getRecordsSortedByAge'](
          null,
          {
            base: { symbol: 'OAX', address: oax.address },
            quote: { symbol: 'WETH', address: weth.address }
          }
        )

        expect(sortedOrders[0].data.sig).to.eql(orderExpiringSooner.sig)
        expect(sortedOrders[1].data.sig).to.eql(signedOrder.sig)
      })

      it('sorts records by age and filter by account', () => {
        store.commit('blotter/addRecord', signedOrder)

        const orderExpiringSooner = {
          ...signedOrder,
          sell: { amount: D`2`, token: weth },
          orderHash: 'sortedFirst'
        }

        store.commit('blotter/addRecord', orderExpiringSooner)

        const sortedOrders = store.getters['blotter/getRecordsSortedByAge'](
          'dummyAddress',
          {
            base: { symbol: 'OAX', address: oax.address },
            quote: { symbol: 'WETH', address: weth.address }
          }
        )

        expect(sortedOrders).to.be.empty // eslint-disable-line
      })

      it('filters records by token pair', () => {
        store.commit('blotter/addRecord', signedOrder)

        const orderExpiringSooner = {
          ...signedOrder,
          sell: { amount: D`2`, token: weth },
          orderHash: 'sortedFirst'
        }

        store.commit('blotter/addRecord', orderExpiringSooner)

        const sortedOrders = store.getters['blotter/getRecordsSortedByAge'](
          signedOrder.address,
          {
            base: { symbol: 'base', address: ZERO_ADDR },
            quote: { symbol: 'quote', address: ZERO_ADDR }
          }
        )

        expect(sortedOrders).to.be.empty // eslint-disable-line
      })
    })

    it('prevents direct mutation of blotter record once committed to store', () => {
      const newSignedOrder = clone(signedOrder)
      Blotter.insert(blotter, newSignedOrder)

      expect(() => {
        newSignedOrder.orderHash = 'invalid-mutation'
      }).to.throw('Do not mutate')
    })
  })

  describe('Persistence', () => {
    it('the blotter state is saved when a record is added', () => {
      store.commit('blotter/addRecord', signedOrder)

      const retrievedOrder = store.getters['blotter/getRecord'](
        signedOrder.orderHash
      )
      const storedBlotter = BlotterStorage.load(blotter.orderStorage)

      expect(storedBlotter[0]).to.eql(retrievedOrder)
    })

    it('state is updated when an order is updated', () => {
      store.commit('blotter/addRecord', signedOrder)
      store.commit('blotter/updateRecord', {
        ...signedOrder,
        buy: { amount: D`2`, token: oax }
      })

      const retrievedOrder = store.getters['blotter/getRecord'](
        signedOrder.orderHash
      )
      const storedBlotter = BlotterStorage.load(blotter.orderStorage)

      expect(retrievedOrder.data.buy.amount).eqD(D`2`)
      expect(storedBlotter[0]).to.eql(retrievedOrder)
    })

    it('restores orders on startup', async () => {
      store.commit('blotter/addRecord', signedOrder)
      const createdRecord = store.getters['blotter/getRecord'](
        signedOrder.orderHash
      )

      // Simulate restarting the client application where the Vuex store will be empty
      blotter = Blotter.stop(blotter)
      store = new Vuex.Store({ strict: true })
      blotter = Blotter.start(
        Blotter.create({
          store,
          storage,
          netId,
          sendMessageChannel: makeChannel(),
          receiveMessageChannel: makeChannel()
        })
      )

      const records = store.state.blotter.orders

      expect(records).lengthOf(1)
      expect(records[0]).to.eql(createdRecord)
    })

    context('restored orders', () => {
      let subscription

      afterEach(() => subscription.unsubscribe())

      it('will expire too', done => {
        store.commit('blotter/addRecord', { ...signedOrder, exp: now() })

        // Simulate restarting the client application where the Vuex store will be empty
        blotter = Blotter.stop(blotter)
        store = new Vuex.Store({})
        blotter = Blotter.start(
          Blotter.create({
            store,
            storage,
            netId,
            sendMessageChannel: makeChannel(),
            receiveMessageChannel: makeChannel()
          })
        )

        subscription = VuexMutationsPayload(
          store,
          'blotter/updateRecord'
        ).subscribe(order => {
          expect(order).has.property('status', 'expired')
          done()
        })
      })
    })
  })
})
