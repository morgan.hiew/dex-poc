/* eslint-env mocha */

import {expect} from 'chain-dsl/test/helpers'
import Eth from 'ethjs'

describe.skip('Ethjs experiments', function () {
  it('should distinguish methods with the same name but different signature',
    function () {
      const ABI = [
        {
          type: 'function',
          name: 'meth',
          inputs: [],
          outputs: [],
          payable: false,
          constant: false
        },
        {
          type: 'function',
          name: 'methx',
          inputs: [{name: 'x', type: 'uint256'}],
          outputs: [],
          payable: false,
          constant: false
        }
      ]

      const eth = new Eth(new Eth.HttpProvider('ws://localhost:8456'))
      const c = eth.contract(ABI).at('0x0')

      expect(c).include.keys(['meth!'])
    })
})
