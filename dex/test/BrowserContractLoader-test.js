/* eslint-env mocha */

import { expect } from 'chain-dsl/test/helpers'
import fetchMock from 'fetch-mock'
import BrowserContractLoader from '../src/BrowserContractLoader'

describe('BrowserContractLoader', function () {
  const contractJsonUrl = 'net/1337/weth.json'

  beforeEach(() => { fetchMock.config.warnOnFallback = false })
  beforeEach(() => {
    fetchMock.get(contractJsonUrl, '{"name": "WETH9"}').catch({
      status: 404,
      body: 'Cannot GET ...',
      sendAsJson: false
    })
  })
  afterEach(fetchMock.restore)

  it('loads existing contract', async () => {
    await expect(BrowserContractLoader('1337')('weth'))
      .eventually.have.property('name', 'WETH9')
  })

  it('throws when contract is missing', async () => {
    await expect(BrowserContractLoader('9')('non-existant'))
      .rejectedWith(/exist.*9\/non-existant.json/)
  })
})
