import {toBN} from "web3-utils"
import {expect} from "./helpers"

describe('test/helpers', function () {
    describe('expect', function () {
        it('handles resolved promises', async () => {
            await expect(Promise.resolve(123)).become(123)
            await expect(Promise.resolve(123)).eventually.eql(123)
        })
        it('handles rejected promises', async () => {
            await expect(Promise.reject(Error('Error: blah...')))
                .rejectedWith(/blah/)
        })
    })

    describe('expect().eq()', function () {
        it('accepts BigNumbers', () => {
            expect(BigNumber('1.23')).eq(BigNumber('1.23'))
        })

        it('actual BN with expected BigNumbers', () => {
            expect(toBN('123')).eq(BigNumber('123'))
        })

        it('actual BN with expected BigNumbers', () => {
            expect(BigNumber('123')).eq(toBN('123'))
        })

        it('actual BN with expected BN', () => {
            expect(toBN('123')).eq(toBN('123'))
        })

        it('does not accept numbers', () => {
            expect(() => expect(123).eq(123)).throw(/provide.*string/i)
        })

        it('accepts decimal precision', () => {
            expect(BigNumber('0.994')).not.eq(BigNumber('0.99'))
            expect(BigNumber('0.994')).eq(BigNumber('0.99'), 2)
        })
    })
})
