import {expect} from "./helpers" // provide globals: BigNumber, BN, toBN
import {D, D2wad, wad2D, floor, ceil, sigCoords} from '../es6'

describe('es6', function () {
    describe('D', function () {
        it("made from template literal", () => expect(D`1.23`).eq(BigNumber('1.23')))

        it("accepts String", () => expect(D('1.23')).eq(BigNumber('1.23')))

        it("rejects Number", () => {
            expect(() => D(1.23)).to.throw(/number.*accurate.*string/i)
        })
    })

    describe('D2wad', function () {
        it("accepts BigNumber", () => {
            expect(D2wad(D`1.23`))
                .to.be.instanceOf(BN)
                .eq(toBN('1' + '230' + '000' + '000' + '000' + '000' + '000'))
        })

        it("accepts String", () => {
            expect(D2wad('1.23'))
                .to.be.instanceOf(BN)
                .eq(toBN('1' + '230' + '000' + '000' + '000' + '000' + '000'))
        })

        it('understands engineering format', () => {
            expect(D2wad('1e-7'))
                .eq(toBN(toBN('100' + '000' + '000' + '000')))
        })

        it("rejects Number", () => expect(() => D2wad(1.23)).throw())
    })

    describe('wad2D', function () {
        it("rejects BigNumber", () => expect(() => wad2D(D`1.23`)).throw())

        it("accepts String", () => {
            expect(wad2D('123'))
                .to.be.instanceOf(BigNumber)
                .eq(D`123e-18`)
        })

        it("accepts BN", () => {
            expect(wad2D(toBN('123')))
                .to.be.instanceOf(BigNumber)
                .eq(D`123e-18`)
        })

        it("rejects Number", () => expect(() => wad2D(1.23)).throw())
    })

    describe("rounding", function (){

        it("ceils", () => {
            expect(ceil("0.9", 0)).eq("1")
            expect(ceil("0.1", 0)).eq("1")
            expect(ceil("0.1", 1)).eq("0.1")
            expect(ceil("0.1", 2)).eq("0.10")
            expect(ceil("0.02", 1)).eq("0.1")
            expect(ceil("0.02", 2)).eq("0.02")
        })

        it("floors", () => {
            expect(floor("1.9", 0)).eq("1")
            expect(floor("1.1", 0)).eq("1")
            expect(floor("1.1", 1)).eq("1.1")
            expect(floor("1.1", 2)).eq("1.10")
            expect(floor("1.02", 1)).eq("1.0")
            expect(floor("1.02", 2)).eq("1.02")
        })

        it("does not make numbers negative", () => {
            expect(floor("0.9", 0)).eq("0")
            expect(floor("0.1", 0)).eq("0")
            expect(floor("0.1", 1)).eq("0.1")
            expect(floor("0.1", 2)).eq("0.10")
            expect(floor("0.02", 1)).eq("0.0")
            expect(floor("0.02", 2)).eq("0.02")
        })
    })

    // TODO
    describe.skip('sigCoords', function () {
        it('works', async () => {
            expect(sigCoords('')).eql({v: 1, r: '', s: ''})
        })

    })
})
