import {expect} from "./helpers"
import {Web3, toBN, waitForConnection, create, web3Contract, call, balance, transfer} from "../index"
import {load} from "../json"
import Debug from "debug"
import solc from "../solc"
import path from "path"

const dbg = Debug(__filename)

describe('chain-dsl', function () {
    let web3, DEPLOYER, Foo, fooInterface, foo

    before(async () => {
        ;({Foo} = solc.flattenJsonOutput(load(path.join(__dirname, '../out/contracts.json'))))
        fooInterface = load(path.join(__dirname, '../net/60/foo.json'))
        web3 = new Web3('http://localhost:8545')
        await waitForConnection(web3)
        ;[DEPLOYER] = await web3.eth.getAccounts()
        foo = web3Contract(web3, fooInterface)
    })

    after(async () => {
        const conn = web3.currentProvider.connection
        if (conn) return conn.close()
    })

    describe('web3Contract', function () {
        it('works', async () => {
            const foo = web3Contract(web3, fooInterface)
            await expect(call(foo, 'bar')).eventually.eql('42')
        })
    })

    describe('create', function () {
        // TODO: Investigate linux failure
        it.skip('works over HTTP', async () => {
            for (let i = 0; i < 80; i++) {
                // dbg(`Deployment #${i}`)
                const foo = await create(web3, DEPLOYER, Foo)
                await expect(call(foo, 'bar')).eventually.eql('42')
            }
        })

        it.skip('works over WebSockets', async () => {
            web3 = new Web3('ws://localhost:8546')
            await waitForConnection(web3)
            ;[DEPLOYER] = await web3.eth.getAccounts()

            let foo
            for (i = 0; i < 100; i++) {
                dbg(`Deployment #${i}`)
                foo = await create(web3, DEPLOYER, Foo)
            }
            await expect(call(foo, 'bar')).eventually.eql('42')
        })
    })

    describe('call', function () {
        it('works over HTTP', async () => {
            await expect(call(foo, 'bar')).eventually.eql('42')
        })

        it.skip('works over Websockets', async () => {
            const web3ws = new Web3('ws://localhost:8546')
            let foo = await create(web3ws, DEPLOYER, Foo)
            await waitForConnection(web3ws)
            for (i = 0; i < 10; i++) {
                dbg(`Deployment #${i}`)
                await expect(call(foo, 'bar')).eventually.eql('42')
            }
        })

        it.skip('throws on error', async () => {
        })

        it.skip('throws on receipt.status !== 1', async () => {
        })
    })

    describe('transfer', function () {
        it('works', async () => {
            const someETH = toBN(Web3.utils.toWei('123', 'wei'))
            const {address: acc} = web3.eth.accounts.create()

            const tx = await transfer(web3, DEPLOYER, acc, someETH)

            expect(tx).have.property('status').eq('0x1')
            expect(await balance(web3, acc)).eq(someETH)
        })

        it.skip('throws', async () => {
            // TODO Make it fail
            const tx = await transfer(web3, DEPLOYER, DEPLOYER, toBN(1))

            expect(tx).have.property('status').eq('0x0')
        })
    })
})
